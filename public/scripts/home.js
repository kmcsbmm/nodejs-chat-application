$('#signout-button').click(() => { 
    $.post("/api/sign-out", () => { 
        location.href = "/sign-in"
    })
})

$('#editaccount-button').click(() => {
    location.href = "/edit"
})

$('#deleteaccount-button').click(() => {
    location.href = "/delete"
})

var socket = io() 

socket.emit('hello', myUsername) 


socket.on('new user', username => {
    let html = $('.list').html() 
    
    html += `
        <div class="listuser" id="user_${username}">${username}</div>
    ` 
    $('.list').html(html) 
})


socket.on('receive message', message => {
    const { username, content } = message 

    let html = $('.chatlog').html()
    html += `
        <div class="chatlogmessage">
            <strong>${username}:</strong> ${content}
        </div>
    `
    $('.chatlog').html(html)

    $('.chatlog')[0].scrollTop = $('.chatlog')[0].scrollHeight 
})

socket.on('receive chatlog', messages => {
    for (let i = messages.length-1; i > 0; i--) { 
        const { username, content } = messages[i]


        let html = $('.chatlog').html()
        html += `
            <div class="chatlogmessage">
                <strong>${username}:</strong> ${content}
            </div>
        `
        $('.chatlog').html(html)
    }
})

socket.on('receive users', users => { 
    users.forEach(user => { 
        let html = $('.list').html()
        html += `
            <div class="listuser" id="user_${user}">${user}</div>
        `
        $('.list').html(html)
    })
})

socket.on('user left', username => {
    $('#user_' + username).remove()
})

$('.send').click(() => {
    const content = $('.body').val() 
    const length = content.length 

    if (length > 0 && length <= 250) { 
        socket.emit('send message', {
            username: myUsername,
            content: content
        }) 
    
        $('.body').val("") 

    }
})



