// Import SQLite modules
const { Database } = require('sqlite3')
const { open } = require('sqlite')

const bcrypt = require('bcrypt') // Import bcrypt jelszó titkosítás
const { v4: uuidv4 } = require('uuid') // uuid import

const { getUserDetails, getIdFromSession } = require('./view')

// Regisztráció
const signUp = async (req, res) => {
    //html id-k alapján való azonosítás
    const { username, password } = req.body

    // account.db létrehozása
    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    // Felhasználónév check
    const usernameTaken = await db.get("SELECT * FROM users WHERE username = ?", username)

    if (usernameTaken) {
        res.status(409).end("A felhasználónév már foglalt.") // 
        return false 
    }

    const hash = await bcrypt.hash(password, 10) // jelszó titkosítás


    // beillesztés a táblázatba
    await db.run("INSERT INTO users (username, password) VALUES (?, ?)", [username, hash])
    await db.close()

    res.redirect('/sign-in') // átírányítás a bejelentkezéshez
    return true
}


// session generálása és adatbázisba való tárolása
const generateSession = async (id) => {
    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    session = uuidv4() 


    // beillesztés
    await db.run("INSERT INTO sessions (id, session) VALUES (?, ?)", [id, session])
    await db.close()

    return session
}

// Bejelentkezés
const signIn = async (req, res) => {
    ///html id-k alapján való azonosítás
    const { username, password } = req.body
        
    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    const userRow = await db.get("SELECT id, password FROM users WHERE username = ?", username) // id és jelszó kiválasztás felhasználónév alapján

    if (!userRow) { // Nem létező felhasznlónév
        res.status(401).end("Helytelen felhasznalonev vagy jelszo.") // 401 "unauthorised" error
        return false
    }

    const { id, password: hash } = userRow // id és jelszó

    await db.close()

    // megnézni hogy a begépelt jelszó egyezik e a hash jelszóval
    const passwordsMatch = await bcrypt.compare(password, hash) 

    if (!passwordsMatch) {
        res.status(401).end("Helytelen felhasznalonev vagy jelszo.")
        return false
    }

    const session = await generateSession(id) // session létrehozása

 
        res.cookie('session', session, {
            httpOnly: true,
        }) // 

    res.redirect('/') // siker,átirányítás

    return true
}






// kijelentkezés 
const signOut = async (req, res) => {
    const session = req.cookies.session // session lekérése

    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    // session kitörlése
    await db.run("DELETE FROM sessions WHERE session = ?", session)
    await db.close()

    res.clearCookie('session') 
    res.end()
}


// Fiók törlése
const deleteAccount = async (req, res) => {
    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    const { verifypassword: verifyPassword } = req.body //id alapján azonosítás verifyPaassword
    const session = req.cookies.session // session lekérése

    const { id, password: hash } = await getUserDetails(session) // bycrypt titkosíítás

    // összehasonlitás
    const verifyPasswordCorrect = await bcrypt.compare(verifyPassword, hash)


    // helytelen jelszó
    if (!verifyPasswordCorrect) {
        res.status(401).end("Helytelen jelszo")
        return false
    }

    // fiók törlése
    await db.run("DELETE FROM users WHERE id = ?", id)
    await db.run("DELETE FROM sessions WHERE id = ?", id)
    await db.close()

    res.clearCookie('session') // session uuid törlése
    res.redirect('/sign-in') // átirányítása bejelentkezéshez
    return true
}

module.exports = {
    signUp,
    signIn,
    signOut,
    deleteAccount
}
