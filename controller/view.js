const { Database } = require('sqlite3')
const { open } = require('sqlite')

// Bejelentkezés form renderelése
const signInPage = (req, res, next) => {
    res.render('sign-in.html')
} 

// regisztráció form renderelése
const signUpPage = (req, res, next) => {
    res.render('sign-up.html')
}

// felhasználónév és session tárolása
const homePage = async (req, res, next) => {
    const session = req.cookies.session 

    const { username } = await getUserDetails(session)

    if (!username) { 
        res.clearCookie('session') /
        res.redirect('/sign-in')
        return
    }
    


    //HomePage render
    res.end(`
    <!DOCTYPE html>
    <html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Home</title>
        <script src="https://cdn.tailwindcss.com"></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
        <script src="https://cdn.socket.io/4.5.4/socket.io.min.js"></script>
        <script>
            const myUsername = "${username}"
        </script>
        <style>
            ::-webkit-scrollbar {
                width: 20px;
            }
            ::-webkit-scrollbar-track {
                background-color: transparent;
            }
            ::-webkit-scrollbar-thumb {
                background-color: #d6dee1;
                border-radius: 20px;
                border: 6px solid transparent;
                background-clip: content-box;
            }
    
        </style>
    </head>
    <body class="flex h-screen bg-gray-200">
        <div class="w-64 bg-white p-3 flex flex-col justify-between">
            <div>
                <h2 class="text-xl font-bold mb-4 text-center">Felhasználók</h2>
                <div class="list"></div>
            </div>
            
            <div class="flex w-full items-center">
                <div class="max-w-[100%] grow">
                
                    <div class="group relative" data-headlessui-state="">
                   
                        <div class="flex w-full max-w-[100%] items-center gap-2 rounded-lg p-3 text-sm p hover:bg-gray-200">
                            <div class="flex-shrink-0">
                                <div class="flex items-center justify-center overflow-hidden rounded-full">
                                </div>
                            </div>
                            
                            <div class="relative -top-px grow -space-y-px truncate text-left text-token-text-primary">
                                <div>${username} 
                                 
                                    
                                    <button style="float: right; " type="button" id="signout-button" >
                                      <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <g id="Interface / Log_Out"> <path id="Vector" d="M12 15L15 12M15 12L12 9M15 12H4M9 7.24859V7.2002C9 6.08009 9 5.51962 9.21799 5.0918C9.40973 4.71547 9.71547 4.40973 10.0918 4.21799C10.5196 4 11.0801 4 12.2002 4H16.8002C17.9203 4 18.4796 4 18.9074 4.21799C19.2837 4.40973 19.5905 4.71547 19.7822 5.0918C20 5.5192 20 6.07899 20 7.19691V16.8036C20 17.9215 20 18.4805 19.7822 18.9079C19.5905 19.2842 19.2837 19.5905 18.9074 19.7822C18.48 20 17.921 20 16.8031 20H12.1969C11.079 20 10.5192 20 10.0918 19.7822C9.71547 19.5905 9.40973 19.2839 9.21799 18.9076C9 18.4798 9 17.9201 9 16.8V16.75" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path> </g> </g></svg>
    
    
    
                                      <button style="float: right;" type="button" id="deleteaccount-button">
    
                                        <svg width="20px" height="20px" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg"><g id="SVGRepo_bgCarrier" stroke-width="0"></g><g id="SVGRepo_tracerCarrier" stroke-linecap="round" stroke-linejoin="round"></g><g id="SVGRepo_iconCarrier"> <path d="M18 6L17.1991 18.0129C17.129 19.065 17.0939 19.5911 16.8667 19.99C16.6666 20.3412 16.3648 20.6235 16.0011 20.7998C15.588 21 15.0607 21 14.0062 21H9.99377C8.93927 21 8.41202 21 7.99889 20.7998C7.63517 20.6235 7.33339 20.3412 7.13332 19.99C6.90607 19.5911 6.871 19.065 6.80086 18.0129L6 6M4 6H20M16 6L15.7294 5.18807C15.4671 4.40125 15.3359 4.00784 15.0927 3.71698C14.8779 3.46013 14.6021 3.26132 14.2905 3.13878C13.9376 3 13.523 3 12.6936 3H11.3064C10.477 3 10.0624 3 9.70951 3.13878C9.39792 3.26132 9.12208 3.46013 8.90729 3.71698C8.66405 4.00784 8.53292 4.40125 8.27064 5.18807L8 6M14 10V17M10 10V17" stroke="#000000" stroke-width="2" stroke-linecap="round" stroke-linejoin="round"></path> </g></svg>
                                </div>
                            </div>
                            
                          </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="flex-1 p-6">
            <div class="bg-white p-12 rounded-md overflow-auto"  style="height: calc(90vh - 60px);">
                <h2 class="text-xl font-bold mb-4">Üzenetek</h2>
                <div class="chatlog inset-0 overflow-y-auto"></div>
            </div>
            <div class="bg-white p-6 mt-4 rounded-md">
                <form class="flex">
                    <input type="text" rows="1" class="block mx-4 p-2.5 w-full text-sm text-gray-900 bg-white rounded-lg border border-gray-300 focus:ring-blue-500 focus:border-blue-500 dark:bg-gray-800 dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500 body" placeholder="Az üzeneted"></input>
                    <button type="button" class="inline-flex justify-center p-2 text-blue-600 rounded-full  send cursor-pointer hover:bg-blue-100 dark:text-blue-500 dark:hover:bg-gray-600">
                        <svg class="h-6 w-6 text-slate-500" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round">
                            <line x1="22" y1="2" x2="11" y2="13" />
                            <polygon points="22 2 15 22 11 13 2 9 22 2" />
                        </svg>
                    </button>
                </form>
            </div>
        </div>
        <script src="scripts/home.js"> 
    
          </script>
          <script>
    
    
    
    
          </script>
    </body>
    </html>
    
    `)
}

// session alapján id lekérdezés
const getUserDetails = async (session) => {
    const db = await open({
        filename: "accounts.db",
        driver: Database
    })

    const sessionRow = await db.get("SELECT id FROM sessions WHERE session = ?", session)

    if (!sessionRow) {
        return false
    }

    const id = sessionRow.id

    const userRow = await db.get("SELECT * FROM users WHERE id = ?", id) // id alapján minden lekérdezés

    if (!userRow) { 
        return false
    }

    await db.close()

    return userRow
}



// fiók törlés
const deletePage = (req, res, next) => {
    const session = req.cookies.session

    if (!session) {
        res.redirect('/sign-in')
        return
    }

    res.render('delete.html')
}

module.exports = { 
    signInPage, 
    signUpPage, 
    homePage,
    deletePage,
    getUserDetails
}
